#!/bin/sh

echo '#!/bin/bash
while [ true ] ; do
	sleep 5;
	brightness=$(cat /sys/class/backlight/intel_backlight/brightness) ;
	if [ $brightness -lt "100" ] ; then
		echo 100 > /sys/class/backlight/intel_backlight/brightness ;
	fi
done' > /usr/local/bin/brightnesswatcher

chmod +x /usr/local/bin/brightnesswatcher

echo "[Unit]
Description=Screen brightness workaround for some Intel based laptops/tablets

[Service]
Type=simple
RemainAfterExit=yes
ExecStart=/usr/local/bin/brightnesswatcher

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/brightnesswatcher.service



systemctl daemon-reload
systemctl enable brightnesswatcher.service
systemctl start brightnesswatcher.service